This project demonstrates some intermediate use cases for interaction in an HTML5 canvas.

The provided files are part of the text "HTML5 Canvas Cookbook" by Eric Rowell.
Additional resources and more information may be found here:
http://www.html5canvastutorials.com/cookbook/