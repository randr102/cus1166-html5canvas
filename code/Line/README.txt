This project is a simple example that introduces the HTML5 canvas element.

The provided file is adopted from the text "HTML5 Canvas Cookbook" by Eric Rowell.
Additional resources and more information may be found here:
http://www.html5canvastutorials.com/cookbook/