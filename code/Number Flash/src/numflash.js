window.onload = function (mask) {
    var canvas = document.getElementById("myCanvas");
    var context = canvas.getContext("2d");

    context.font = "40pt Calibri";
    context.fillStyle = "black";

    context.textAlign = "center";
    context.textBaseline = "middle";

    context.fillText("Get ready...", canvas.width / 2, 120);
    window.setTimeout(function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        // prepare game

        // random number from 1000 to 9999
        var min = 1000;
        var max = 9999;
        var hiddenNumber = Math.floor(Math.random() * (max - min + 1)) + min;

        context.fillText(hiddenNumber, canvas.width / 2, 120);
        window.setTimeout(function() {
            context.clearRect(0, 0, canvas.width, canvas.height);
            setTimeout(function() {
                var answer = prompt("What was the number?");
                if (answer == hiddenNumber) {
                    alert("Yes!");
                } else {
                    alert("No! The number was " + hiddenNumber + ".");
                }
            }, 1000);

        }, 300);


    }, 5000);




};